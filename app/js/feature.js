(function($){
  'use strict'; // Start of use strict

  	/* --------------------------------------------
    	DOCUMENT.READY
  		--------------------------------------------- */
  		$(document).ready(function(){

  			/* --------------------------------------------
		      FULL PAGE SECTIONS
		    	--------------------------------------------- */
		      //$('#fullpage').fullpage();

		  	/* -------------------------------------
		      SMOKE JS
		    	------------------------------------- 
		      var canvas = document.getElementById('canvas')
			  var ctx = canvas.getContext('2d')
			  canvas.width = innerWidth
			  canvas.height = innerHeight

			  var party = smokemachine(ctx, [160, 160, 2])
			  party.start() // start animating

			  onmousemove = function (e) {
			    var x = e.clientX
			    var y = e.clientY
			    var n = .5
			    var t = Math.floor(Math.random() * 200) + 3800
			    party.addsmoke(x, y, n, t)
			  }

			  setInterval(function(){
			    party.addsmoke(innerWidth/2, innerHeight, 1)
			  }, 100)*/

		  	/* -------------------------------------
		  		ANIMATED TEXT
		    	------------------------------------- */
			    $('#js-rotating').Morphext({
			    // The [in] animation type. Refer to Animate.css for a list of available animations.
			    animation: 'bounceIn',
			    // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
			    separator: ',',
			    // The delay between the changing of each phrase in milliseconds.
			    speed: 3000,
			    complete: function () {
			      // Called after the entrance animation is executed.
			    }
			  });

		  	/* -------------------------------------
		  		ANIMATED SPERM SHOES
		    	------------------------------------- */
			    $('#slideshow > div:gt(0)').hide();

			      setInterval(function() {
			        $('#slideshow > div:first')
			          .fadeOut(1000)
			          .next()
			          .fadeIn(1000)
			          .end()
			          .appendTo('#slideshow');
			      }, 1000);

			/* -------------------------------------
		  		ANIMATED DYNAMIC SHOES
		    	------------------------------------- */
			    $('#slideshowDynamicShoes').reel({
			        images:      'images/dynamicShoe/##.png|1..60|1',
			        cw:          false,
			        frame:       1,
			        frames:      60,
			        speed:       -0.1,
			        velocity:    0,
			        duration:    6,
			        brake:       0.23,
			        laziness:    0,
			        delay:       5,
			        responsive:  true,
			        loops:       true,
			        cursor:      'hand', 
			        timeout:     2        
			      });

			/* -------------------------------------
		  		OWL
		    	------------------------------------- */

				/*$('.owl-carousel').owlCarousel({
				    loop:true,
				    margin:10,
				    nav:true,
				    responsive:{
				        0:{
				            items:1
				        },
				        600:{
				            items:3
				        },
				        1000:{
				            items:5
				        }
				    }
				});*/

				/*navigation: true,
  navigationText: ["", ""],
  slideSpeed: 300,
  paginationSpeed: 400,
  autoPlay: true,
  mouseDrag: true,
  singleItem: true,
  animateIn: 'fadeIn', // add this
  animateOut: 'fadeOut' // and this*/

				$('.owl-carousel').owlCarousel({
					/*loop:true,
					navigation: true,
					navigationText: ["", ""],*/
					slideSpeed: 300,
					paginationSpeed: 400,
					autoPlay: true,
					mouseDrag: true,
					singleItem: true,
					margin:10,
    			items:12,
					dots:true,
					animateIn: 'fadeIn', 
  				animateOut: 'fadeOut' ,
					nav: true,
					navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
					responsiveClass:true,
					responsive:{
							0:{
									items:1,
									nav:true,
									dots:true
							},
							600:{
									items:1,
									nav:true,
									dots:true
							},
							1000:{
									items:1,
									nav:true,
									loop:true,
									dots:true
							}
					}
			});


			/* -------------------------------------
		  		MAGNIFIC POPUP 
		    	-------------------------------------*/
		      $('.lightbox').magnificPopup({
		       // delegate: 'a',
		        type: 'image',
		        mainClass: 'mfp-3d-unfold',
		        removalDelay: 500, //delay removal by X to allow out-animation
		        callbacks: {
		        beforeOpen: function() {
		          // just a hack that adds mfp-anim class to markup 
		           this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
		          // this.st.mainClass = this.st.el.attr('data-effect');
		        }
		        },
		        closeOnContentClick: true,
		        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
		      });


		    /* --------------------------------------------
			    WOW ANIMATE
			 	--------------------------------------------- 
			  function initWow(){
			    var wow = new WOW( { mobile: false, } );
			    wow.init();
			  }*/
			  new WOW().init();
			  




  	});

  	

	/* --------------------------------------------
      FUNCTIONS
    --------------------------------------------- */
	    //initMenu();

	    /*if ( $('#nav').length ){
	      initAffixCheck();
	    };
	    if ( $('.mobile-transparent').length ){
	      initMobTranspFix();
	    };

	    if ( $('.owl-plugin').length ){
	      initPageSliders(); 
	    };

	    if ( $('#flickr-feeds').length ){
	      initFlickrFeeds();
	    };
	    if ( $('#twitter-feeds').length ){
	      initTwitterFeeds();
	    };
	    
	    if ( $('#items-grid').length ){
	      initWorkFilter();
	    };
	    if ( $('.masonry').length ){
	      initMasonry();
	    };
	    if ( $('.wow').length ){
	      initWow(); 
	    };
	     
	    if ( $('.owl-plugin').length ){
	      initPageSliders(); 
	    };
	    if ( $('.mfp-plugin').length ){
	      initMagnPopup(); 
	    };
	    if ( $('.js-height-fullscr').length ){
	      initImgHeight();
	    };
	    if ( $('.count-number').length ){
	      initCounters();
	    }; 
	    if ( $('#header-left-nav').length ){
	      initLeftMenu();
	    };
	    if ( $('#google-map').length ){
	      initMap();
	    };*/



	    


	    



















})(jQuery); // End of use strict